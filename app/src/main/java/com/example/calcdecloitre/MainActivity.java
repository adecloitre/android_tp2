package com.example.calcdecloitre;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    public void clearValue(View maVue) {

        EditText editText1 = findViewById(R.id.value1);
        EditText editText2 = findViewById(R.id.value2);

        editText1.setText("");
        editText2.setText("");
    }

    public void exit(View maVue) {

        finish();
    }

    public void calculation(View maVue) {

        EditText editText1 = findViewById(R.id.value1);
        EditText editText2 = findViewById(R.id.value2);

        if(!editText1.getText().toString().equals("") && !editText2.getText().toString().equals("")){

            int num1 = Integer.parseInt(editText1.getText().toString());

            int num2 = Integer.parseInt(editText2.getText().toString());


            TextView result = findViewById(R.id.result);

            RadioButton radioButtonPlus = findViewById(R.id.plus);
            RadioButton radioButtonMoins = findViewById(R.id.moins);
            RadioButton radioButtonMultiplie = findViewById(R.id.multiplie);
            RadioButton radioButtonDivise = findViewById(R.id.divise);

            if(radioButtonPlus.isChecked()){
                result.setText(String.valueOf(num1+num2));

            } else if (radioButtonMoins.isChecked()) {
                result.setText(String.valueOf(num1-num2));

            } else if (radioButtonMultiplie.isChecked()) {
                result.setText(String.valueOf(num1*num2));

            } else if (radioButtonDivise.isChecked()) {
                if(num2 == 0){
                    result.setText("null division");
                }else{
                    result.setText(String.valueOf(num1/num2));
                }

            }
        }else{
            System.out.println("champs non rempli");
        }
    }
}